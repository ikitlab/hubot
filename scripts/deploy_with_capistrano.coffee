# Description:
#  Deployment with capistrano
#
# Dependencies:
#
# Configuration:
#
# Commands:
#   deploy <repo>
#   what can you deploy?
#
## Authors:
#   Travis Berry
#

#array of the repo names to match and deploy
repos = [
  "test-repo"
]

module.exports = (robot) ->
  robot.respond /deploy (.+)/i, (msg) ->
    if msg.match[1] in repos
      #send waiting messages
      msg.send 'Attempting deploy. Please hold.'

      #hit the sinatra app to do the deploy
      msg.http("http://localhost:4567/deploy/#{msg.match[1]}")
      .get() (err, res, body) ->
        if res.statusCode == 404
          msg.send 'Something went horribly wrong'
        else
          msg.send 'Deployed like a boss.'
    else
      msg.send 'Nope. I dont know what that is. Try deploying one of these: ' + repos.join(", ")

  robot.respond /(what can you deploy?)/i, (msg) ->
    msg.send 'I can deploy the shit out of ' + repos.join(", ")
